public class Eleven {
  public static void main(String[] args) {
    System.out.println("".isEmpty());
    System.out.println("test".isEmpty());
    System.out.println(" ".isEmpty());
    System.out.println("".isBlank());
    System.out.println("test".isBlank());
    System.out.println(" ".isBlank());
    System.out.println(" _".isBlank());
    System.out.println(" \t\n".isBlank());

    System.out.println(" \tt est\ttest ".strip());
    System.out.println(" \tt est\ttest ".stripLeading());
    System.out.println(" \tt est\ttest ".stripTrailing());

    System.out.println(" test ".repeat(3));

    //TODO: StringJoiner (for toString methods of classes)
    //var myString = "var is a lazy bunch of syntax sugar to me";
    //List myList = new ArrayList<String>();
  }
}
