public class Twelve {

  public static void main(String[] args) {
    //First of all, don't use switches....
    demonstrateTraditionalSwitchStatement();
    demonstrateEnhancedSwitchStatement();
    demonstrateSwitchExpressionWithBreaks(); //==> Please do NOT use this version!!!
    demonstrateSwitchExpressionWithArrows();
    demonstrateLabelRulesWithSharedCases();
//    String oldSkool= "Let's use\n" +
//      " more\n" +
//      " then 1 line!";
//    String newStyle= `Let's use
//       more
//       then 1 line!`;
//    System.out.println(newStyle);
  }

  private static void demonstrateTraditionalSwitchStatement() {

    System.out.println("Traditional Switch Statement:");
    final int integer = 3;
    String numericString;
    switch (integer) {
      case 1:
        numericString = "one";
        break;
      case 2:
        numericString = "two";
        break;
      case 3:
        numericString = "three";
        break;
      default:
        numericString = "N/A";
    }
    System.out.println("\t" + integer + " = " + numericString);
  }

  /**
   * Demonstrate enhanced switch statement used to assign
   * a local variable.
   */
  private static void demonstrateEnhancedSwitchStatement() {
    System.out.println("Enhanced Switch Statement:");
    final int integer = 2;
    String numericString;
    switch (integer) {
      case 1 -> numericString = "one";
      case 2 -> numericString = "two";
      case 3 -> numericString = "three";
      default -> numericString = "N/A";
    }
    System.out.println("\t" + integer + " ==> " + numericString);
  }

  /**
   * Demonstrate switch expression using colons and breaks.
   */
  private static void demonstrateSwitchExpressionWithBreaks() {
    final int integer = 1;
    System.out.println("Switch Expression with Colons/Breaks:");
    final String numericString =
      switch (integer)
        {
          case 1 :
            break "uno"; //this "break result;" freaks me out, why would anyone use this?
          case 2 :
            break "dos";
          case 3 :
            break "tres";
          default :
            break "N/A";
        };
    System.out.println("\t" + integer + " ==> " + numericString);
  }

  /**
   * Demonstrate switch expressions using "arrow" syntax.
   */
  private static void demonstrateSwitchExpressionWithArrows() {
    final int integer = 4;
    System.out.println("Switch Expression with Arrows:");
    final String numericString =
      switch (integer)
        {
          case 1 -> "uno";
          case 2 -> "dos";
          case 3 -> "tres";
          case 4 -> "quatro";
          default -> "N/A";
        };
    System.out.println("\t" + integer + " ==> " + numericString);
  }

  /**
   * Demonstrate that multiple constants can be associated with
   * a single {@code case} and used in conjunction with a
   * {@code switch} expression that uses the "arrow" syntax.
   */
  private static void demonstrateLabelRulesWithSharedCases()  {
    final int integer = 7;
    System.out.println("Multiple Case Labels:");
    final String numericString =
      switch (integer)
        {
          case 0 -> "zero";
          case 1, 3, 5, 7, 9 -> "odd";
          case 2, 4, 6, 8, 10 -> "even";
          default -> "N/A";
        };
    System.out.println("\t" + integer + " ==> " + numericString);

  }

  /**
   * Demonstrate that multiple constants can be associated with
   * a single {@code case} and used in conjunction with a
   * {@code switch} statement that uses the traditional colon and
   * {@code break} syntax.
   */
  public static void demonstrateBlockedStatementsWithSharedCases() {
    final int integer = 6;
    System.out.println("Multiple Case Labels:");
    String numericString;
    switch (integer)
    {
      case 0:
        numericString = "zero";
        break;
      case 1, 3, 5, 7, 9:
        numericString = "odd";
        break;
      case 2, 4, 6, 8, 10:
        numericString = "even";
        break;
      default:
        numericString = "N/A";
    };
    System.out.println("\t" + integer + " ==> " + numericString);
  }
}
